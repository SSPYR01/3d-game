package water;

import org.lwjgl.util.vector.Matrix4f;

import entities.Camera;
import entities.Light;
import shaders.ShaderProgram;
import toolbox.Maths;

public class WaterShader extends ShaderProgram {

	private final static String VERTEX_FILE = "src/water/waterVertex.vert";
	private final static String FRAGMENT_FILE = "src/water/waterFragment.frag";

	private int location_modelMatrix;
	private int location_viewMatrix;
	private int location_projectionMatrix;
	private int location_reflectionTexture;
	private int location_refractionTexture;
	private int location_dudvMap;
	private int location_moveFactor;
	private int location_cameraPos;
	private int location_normalMap;
	private int location_lightCol;
	private int location_lightPos;
	private int location_depthMap;;



	public WaterShader() {
		super(VERTEX_FILE, FRAGMENT_FILE);
	}

	@Override
	protected void bindAttributes() {
		bindAttribute(0, "position");
	}

	@Override
	protected void getAllUniformLocations() {
		location_lightCol = super.getUniformLocation("lightCol");
		location_lightPos = super.getUniformLocation("lightPos");
		location_projectionMatrix = getUniformLocation("projectionMatrix");
		location_normalMap = super.getUniformLocation("normalMap");
		location_viewMatrix = getUniformLocation("viewMatrix");
		location_moveFactor = super.getUniformLocation("moveFactor");
		location_modelMatrix = getUniformLocation("modelMatrix");
		location_reflectionTexture = super.getUniformLocation("reflectionTexture");
		location_refractionTexture = super.getUniformLocation("refractionTexture");
		location_dudvMap = super.getUniformLocation("dudvMap");
		location_cameraPos = super.getUniformLocation("cameraPos");
		location_depthMap = super.getUniformLocation("depthMap");
	}
	
	public void loadProjectionMatrix(Matrix4f projection) {
		loadMatrix(location_projectionMatrix, projection);
	}
	
	public void loadMoveFactor(float moveFactor){
		super.loadFloat(location_moveFactor, moveFactor);
	}
	
	public void loadLight(Light sun){
		super.loadVector(location_lightCol, sun.getColour());
		super.loadVector(location_lightPos, sun.getPosition());
	}
			
	public void connectTextureUnits(){
		super.loadInt(location_reflectionTexture, 0);
		super.loadInt(location_refractionTexture, 1);
		super.loadInt(location_dudvMap, 2);
		super.loadInt(location_normalMap, 3);
		super.loadInt(location_depthMap, 4);
	}
	
	public void loadViewMatrix(Camera camera){
		Matrix4f viewMatrix = Maths.createViewMatrix(camera);
		loadMatrix(location_viewMatrix, viewMatrix);
		super.loadVector(location_cameraPos, camera.getPosition());
	}
	
	public void loadModelMatrix(Matrix4f modelMatrix){
		loadMatrix(location_modelMatrix, modelMatrix);
	}

}
