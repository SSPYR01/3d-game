#version 400 core

in vec4 clipSpace;
in vec2 textureCoords;
in vec3 toCamVec;
in vec3 fromLightVec;

out vec4 out_Color;

uniform sampler2D reflectionTexture;
uniform sampler2D refractionTexture;
uniform sampler2D dudvMap;
uniform float moveFactor; 
uniform sampler2D normalMap;
uniform sampler2D depthMap;

uniform vec3 lightCol;


const float waveStr = 0.04;
const float shineDamper = 20.0f;
const float reflectivity = 0.5f;

void main(void) {

	vec2 ndc = (clipSpace.xy/clipSpace.w) /2 + 0.5;
	vec2 refractCoords = vec2(ndc.x, ndc.y);
	vec2 reflectCoords = vec2(ndc.x, -ndc.y);
	
	float depth = texture(depthMap, refractCoords).r;
	float near = 0.1;
	float far = 1000;
	
	float floorDistance = 2.0 * near * far / (far + near - (2.0 * depth - 1.0) * (far - near));
	
	depth = gl_FragCoord.z;
	float waterDistance = 2.0 * near * far / (far + near - (2.0 * depth - 1.0) * (far - near));
	
	float waterDepth = floorDistance - waterDistance;
	
	vec2 distortedTexCoords = texture(dudvMap, vec2(textureCoords.x + moveFactor, textureCoords.y)).rg*0.1;
	distortedTexCoords = textureCoords + vec2(distortedTexCoords.x, distortedTexCoords.y + moveFactor);
	vec2 totalDistortion = (texture(dudvMap, distortedTexCoords).rg *2.0 - 1.0) * waveStr * clamp(waterDepth/15 , 0.0 , 1.0);
	
	
	refractCoords += totalDistortion;
	refractCoords = clamp(refractCoords, 0.001, 0.999);
	
	reflectCoords += totalDistortion;
	reflectCoords.x = clamp(reflectCoords.x, 0.001, 0.999);
	reflectCoords.y = clamp(reflectCoords.y, -0.999, -0.001);
	
	
	vec4 reflectCol = texture(reflectionTexture, reflectCoords);
	vec4 refractCol = texture(refractionTexture, refractCoords);
	
	vec4 normalMapCol = texture(normalMap, distortedTexCoords);
	vec3 normal = vec3(normalMapCol.r * 2.0 - 1.0, normalMapCol.b * 3.0, normalMapCol.g*2.0-1.0);
	normal = normalize(normal);
	
	
	vec3 viewVec = normalize(toCamVec);
	float refractiveFactor = dot(viewVec, normal);
	//change the pow to change reflectivity 
	refractiveFactor = pow(refractiveFactor, 0.5);
	refractiveFactor = clamp(refractiveFactor, 0.0, 1.0);
	

	vec3 reflectedLight = reflect(normalize(fromLightVec), normal);
	float specular = max(dot(reflectedLight, viewVec), 0.0);
	specular = pow(specular, shineDamper);
	vec3 specularHighlights = lightCol * specular * reflectivity * clamp(waterDepth/3 , 0.0 , 1.0);

	out_Color = mix(reflectCol, refractCol, refractiveFactor);
	out_Color = mix(out_Color, vec4(0.0,0.3,0.5,1.0), 0.2) + vec4(specularHighlights,0.0) ;
	out_Color.a = clamp(waterDepth/3 , 0.0 , 1.0);

}